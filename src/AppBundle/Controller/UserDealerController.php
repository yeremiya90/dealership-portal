<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserDealer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Userdealer controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @Route("users/dealer")
 */
class UserDealerController extends Controller
{
    /**
     * Lists all userDealer entities.
     *
     * @Route("/", name="userdealer_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userDealers = $em->getRepository('AppBundle:UserDealer')->findAll();

        return $this->render('userdealer/index.html.twig', array(
            'userDealers' => $userDealers,
        ));
    }

    /**
     * Creates a new userDealer entity.
     *
     * @Route("/new", name="userdealer_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $userDealer = new Userdealer();
        $form = $this->createForm('AppBundle\Form\UserDealerType', $userDealer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userDealer);
            $em->flush();

            return $this->redirectToRoute('userdealer_show', array('id' => $userDealer->getId()));
        }

        return $this->render('userdealer/new.html.twig', array(
            'userDealer' => $userDealer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a userDealer entity.
     *
     * @Route("/{id}", name="userdealer_show")
     * @Method("GET")
     */
    public function showAction(UserDealer $userDealer)
    {
        $deleteForm = $this->createDeleteForm($userDealer);

        return $this->render('userdealer/show.html.twig', array(
            'userDealer' => $userDealer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing userDealer entity.
     *
     * @Route("/{id}/edit", name="userdealer_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, UserDealer $userDealer)
    {
        $deleteForm = $this->createDeleteForm($userDealer);
        $editForm = $this->createForm('AppBundle\Form\UserDealerType', $userDealer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('userdealer_edit', array('id' => $userDealer->getId()));
        }

        return $this->render('userdealer/edit.html.twig', array(
            'userDealer' => $userDealer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a userDealer entity.
     *
     * @Route("/{id}", name="userdealer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, UserDealer $userDealer)
    {
        $form = $this->createDeleteForm($userDealer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($userDealer);
            $em->flush();
        }

        return $this->redirectToRoute('userdealer_index');
    }

    /**
     * Creates a form to delete a userDealer entity.
     *
     * @param UserDealer $userDealer The userDealer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UserDealer $userDealer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('userdealer_delete', array('id' => $userDealer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
