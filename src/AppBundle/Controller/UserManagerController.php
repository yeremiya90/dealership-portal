<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Usermanager controller.
 * @Route("users/manager")
 */
class UserManagerController extends Controller
{
    /**
     * Lists all uSerManager entities.
     *
     * @Route("/", name="usermanager_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $uSerManagers = $em->getRepository('AppBundle:UserManager')->findAll();

        return $this->render('usermanager/index.html.twig', array(
            'uSerManagers' => $uSerManagers,
        ));
    }

    /**
     * Creates a new uSerManager entity.
     *
     * @Route("/new", name="usermanager_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $uSerManager = new Usermanager();
        $form = $this->createForm('AppBundle\Form\UserManagerType', $uSerManager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($uSerManager);
            $em->flush();

            return $this->redirectToRoute('usermanager_show', array('id' => $uSerManager->getId()));
        }

        return $this->render('usermanager/new.html.twig', array(
            'uSerManager' => $uSerManager,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a uSerManager entity.
     *
     * @Route("/{id}", name="usermanager_show")
     * @Method("GET")
     */
    public function showAction(UserManager $uSerManager)
    {
        $deleteForm = $this->createDeleteForm($uSerManager);

        return $this->render('usermanager/show.html.twig', array(
            'uSerManager' => $uSerManager,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing uSerManager entity.
     *
     * @Route("/{id}/edit", name="usermanager_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, UserManager $uSerManager)
    {
        $deleteForm = $this->createDeleteForm($uSerManager);
        $editForm = $this->createForm('AppBundle\Form\UserManagerType', $uSerManager);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('usermanager_edit', array('id' => $uSerManager->getId()));
        }

        return $this->render('usermanager/edit.html.twig', array(
            'uSerManager' => $uSerManager,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a uSerManager entity.
     *
     * @Route("/{id}", name="usermanager_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, UserManager $uSerManager)
    {
        $form = $this->createDeleteForm($uSerManager);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($uSerManager);
            $em->flush();
        }

        return $this->redirectToRoute('usermanager_index');
    }

    /**
     * Creates a form to delete a uSerManager entity.
     *
     * @param UserManager $uSerManager The uSerManager entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UserManager $uSerManager)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usermanager_delete', array('id' => $uSerManager->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
