<?php


namespace AppBundle\Controller;

use AppBundle\Form\ResetPasswordType;
use AppBundle\Utils\Util;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\UserDealer;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @property  container
 * @Route("/vincheck")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_MANAGER') or has_role('ROLE_USER') ")
 */
class VinCheckController extends Controller
{

    /**
     * @Route("/", name="vincheck_index")
     */
    public function indexAction(Request $request)
    {

        $userEnter = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($userEnter->getRoles()[0] === 'ROLE_USER') {
            /** @var UserDealer $user */
            $user = $em->getRepository('AppBundle:UserDealer')->find($userEnter->getId());
            $editForm = $this->createForm('AppBundle\Form\UserDealerType', $user);
        } else if ($userEnter->getRoles()[0] === 'ROLE_MANAGER') {
            $editForm = $this->createForm('AppBundle\Form\EditUserManagerWithFotoType', $userEnter);
        } else {
            $editForm = $this->createForm('AppBundle\Form\UserType', $userEnter);
        }

        $editForm->handleRequest($request);
        $form = $this->createForm(ResetPasswordType::class, $userEnter);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $oldPassword = $userEnter->getOldPassword();
            $encoder = $this->get('security.password_encoder');
            if (!$encoder->isPasswordValid($userEnter, $oldPassword)) {
                return new JsonResponse('The old password you have given is incorrect. Please try again.');
            }
            $em = $this->getDoctrine()->getManager();
            $encoder = $this->get('security.password_encoder');
            $userEnter->setPassword($encoder->encodePassword($userEnter, $userEnter->getPlainPassword()));
            $em->persist($userEnter);
            $em->flush();
            return new JsonResponse('Your password has been successfully changed.');
        }

        return $this->render('vincheck/index.html.twig', array(
            'user' => $userEnter,
            'edit_form' => $editForm->createView(),
            'form_pass' => $form->createView()
        ));
    }


    /**
     *
     * @Route("/check/products/vin", name="vincheck_product_vin")
     */
    public function checkProductsAction(Request $request)
    {
        $vin = $request->get('vin');
        $util = new Util();
        $url = $this->container->getParameter('url_for_product');
        $key = $this->container->getParameter('product_key');
        $vincheckKey = ["key: " . $key];
        $vincheckVin = ["vin" => $vin];
        $sendReques = $util->HTTPQuery($url, $vincheckVin, $vincheckKey);
        return new JsonResponse($sendReques);
    }

    /**
     *
     * @Route("/check/mmi/vin", name="vincheck_mmi_vin")
     */
    public function checkMMIAction(Request $request)
    {
        $vin = $request->get('vin');
        $util = new Util();
        $url = $this->container->getParameter('url_for_mmi');
        $key = $this->container->getParameter('mmi_key');
        $productId = $this->container->getParameter('mmi_product');
        $vincheckKey = ["key: " . $key];
        $vincheckVin = ["vin" => $vin, "productId" => $productId];
        $sendReques = $util->HTTPQuery($url, $vincheckVin, $vincheckKey);
        return new JsonResponse($sendReques);
    }


    /**
     *
     * @Route("/send/mistake", name="vincheck_send_mistake")
     */
    public function sendMistakeAction(Request $request)
    {
        $vin = $request->get('vin');
        $message = $request->get('message');
        $name = $request->get('name');
        $mistakeEmail = $this->container->getParameter('mistake_email');
        try {
            $this->get('app_mailer')->sendMistake($mistakeEmail, $name, $vin, $message);
            return new JsonResponse('Report sent. Thank you for helping us improve this tool.');
        } catch (Exception $e) {
            return new JsonResponse('Error sending report. Please try again.');
        }

    }
}
