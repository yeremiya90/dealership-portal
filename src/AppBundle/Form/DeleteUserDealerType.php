<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use libphonenumber\PhoneNumberFormat;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeleteUserDealerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dealer', EntityType::class, [
                'class' => 'AppBundle:UserDealer',
                'choice_label' => 'company',
                'label' => false,
                'placeholder' => 'Choose dealer',
                'required' => true,
                'empty_data' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-control required'),
                'query_builder' => function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')
                        ->select('c')
                        ->leftJoin('AppBundle:UserDealer', 'co', 'WITH', 'co.company = c.company')
                        ->orderBy('co.company', 'ASC');
                }



            ]);


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UserDealer',

        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_delete_userdealer';
    }


}
