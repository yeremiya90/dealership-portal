<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use libphonenumber\PhoneNumberFormat;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeleteUserManagerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('manager', EntityType::class, [
                'class' => 'AppBundle:UserManager',
                'choice_label' => 'username',
                'label' => false,
                'placeholder' => 'Choose manager',
                'required' => true,
                'empty_data' => null,
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-control required'),
                'query_builder' => function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('c')
                        ->select('c')
                        ->leftJoin('AppBundle:UserManager', 'co', 'WITH', 'co.username = c.username')
                        ->orderBy('co.username', 'ASC');
                }


            ]);


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UserManager'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_delete_usermanager';
    }


}
