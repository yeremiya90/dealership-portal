<?php

namespace AppBundle\Mailer;

use \Swift_Mailer;
use \Swift_Message;
use \Twig_Environment;
use \AppBundle\Utils\UniqueNameGenerator;

class BaseMailer
{

    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var string
     */
    protected $fromEmail;


    /**
     * @param Swift_Mailer $mailer
     * @param Twig_Environment $twig
     * @param string $fromEmail
     */
    public function __construct(Swift_Mailer $mailer, Twig_Environment $twig, $fromEmail)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->fromEmail = $fromEmail;
    }

    /**
     *
     * @param string $templateName
     * @param array $context
     * @param string|array $toEmail
     *
     * @return void
     */
    protected function send($templateName, array $context, $toEmail)
    {

        $context = $this->twig->mergeGlobals($context);
        /* @var $template \Twig_Template */
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        $context['title'] = $subject;
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);
        $message = Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->fromEmail)
            ->setReplyTo('dealers@bimmer-tech.net')
            ->setTo($toEmail);
        $headers = $message->getHeaders()->get('Message-ID');
        $headers->setId(UniqueNameGenerator::generate() . '@bimmer-tech.net');

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }
        $this->mailer->send($message);

    }

}
