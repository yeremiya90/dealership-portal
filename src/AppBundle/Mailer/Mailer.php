<?php

namespace AppBundle\Mailer;

class Mailer extends BaseMailer
{

    public function sendRegisterConfirm($toEmail, $token)
    {
        $this->send('mail/register-confirm.html.twig', ['token' => $token], $toEmail);
    }

    public function sendResetPassword($toEmail, $token)
    {
        $this->send('mail/reset-password.html.twig', ['token' => $token], $toEmail);
    }

    public function sendTicketReplay($toEmail, $replay)
    {
        $this->send('mail/replay.html.twig', ['replay' => $replay], $toEmail);
    }

    public function sendMistake($toEmail, $name, $vin, $message)
    {
        $this->send('mail/spot-mistake.html.twig', ['name' => $name, 'vin' => $vin, 'message' => $message], $toEmail);
    }


    public function sendStatusChange($toEmail, $date, $name, $company, $status, $newstatus, $discount, $newdiscount)
    {
        $this->send('mail/status-changed.html.twig', ['name' => $name, 'date' => $date, 'company' => $company,
            'status' => $status, 'newstatus' => $newstatus, 'discount' => $discount, 'newdiscount' => $newdiscount], $toEmail);
    }

    public function sendCommentToArticle($toEmail, $article, $message, $mail)
    {
        $this->send('mail/reply-artice.html.twig', ['article' => $article, 'mail' => $mail, 'message' => $message], $toEmail);
    }
}




