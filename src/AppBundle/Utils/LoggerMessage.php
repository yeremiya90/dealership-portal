<?php


namespace AppBundle\Utils;


use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class LoggerMessage
{
    private $log;

    public function __construct()
    {
        $this->log = new Logger('name');
    }

    public function sendInfoMessage($message, $params = null)
    {
        $this->log->pushHandler(new StreamHandler('login.log', Logger::INFO));
        $this->log->info($message, ['email' => $params]);
    }


}