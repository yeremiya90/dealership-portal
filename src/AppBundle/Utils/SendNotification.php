<?php


namespace AppBundle\Utils;


use AppBundle\Entity\Notification;

class SendNotification
{

    function send($notificationToken, $id, $title, $message)
    {
        $notification = new Notification();
        $notification->setMessage($message);
        $notification->setTitle($title);

        $serverFCM = 'https://fcm.googleapis.com/fcm/send';
        $key = 'AAAAKkTnTR0:APA91bFQBj8AixKPBbNroWYpdutu2ZjOt9SyIXt_0AKJJgJ_LV23EfrZp4HMR5JNeGUtqhckMa-d8kVtU9bLGOUYg6ir8YLxQmPVvgDnULVmYJnKY_fA7CKRfA-F00BYVukm3FfIDL5A';
        $msg = [
            'title' => $title,
            'body' => $message,
            "icon" => "https://dealer.bimmer-tech.net/img/logoImg.png",


        ];
        $fields = [
            'to' => $notificationToken,
            'notification' => $msg,
            'data' => ['id' => $id]
        ];

        $headers = [
            'Authorization: key=' . $key,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serverFCM);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        return $notification;
    }

}