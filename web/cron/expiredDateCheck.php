<?php


class SendRequest
{

    private $ch;

    public function __construct()
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:11.0) Gecko/20100101 Firefox/11.0');
    }


    public function HTTPQuery($url, $postdata = NULL, $headers = NULL, $cookies = NULL)
    {
        if ($postdata) {
            curl_setopt($this->ch, CURLOPT_POST, true);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postdata);
        } else {
            curl_setopt($this->ch, CURLOPT_POST, false);
        }
        if ($headers) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
        }
        if ($cookies) {
            curl_setopt($this->ch, CURLOPT_COOKIE, $cookies);
        }
        curl_setopt($this->ch, CURLOPT_URL, $url);
        $data = curl_exec($this->ch);

        return $data;

    }
}


$send = new SendRequest();

$domain = $_SERVER['SERVER_NAME'];
//$domain = 'https://dealer.bimmer-tech.net';
$url = $domain . '/cron/run/check/expiredDate';
$send->HTTPQuery($url);