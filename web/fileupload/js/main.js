$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '../uploads/download/index.php'

    });

    // // Enable iframe cross-domain access via redirect option:
    // $('#fileupload').fileupload(
    //     'option',
    //     'redirect',
    //     window.location.href.replace(
    //         /\/[^\/]*$/,
    //         '/cors/result.html?%s'
    //     )
    // );


    // //Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]

    }).always(function () {
        $(this).removeClass('fileupload-processing');

    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {
                result: result

            });
    });


});


$(document).ready(function () {
    getDealers();
    newFilesToServer();
});

var dealers = null;

function getDealers() {
    $.ajax({
        url: 'add_new_file/dealers',
        method: 'POST',
        timeout: 5000,
        error: function () {
            console.log('error');
        },
        success: function (data) {
            dealers = (data);
            return dealers;
        }
    });

}


$('#fileupload').on("click", ".addDealerToFile", function () {
    var id = $(this).data('id');
    setDealer(id);
});

function setDealer(id) {

    if (dealers === null) {
        dealers = getDealers();
    }
    var selectId = '#selectDealer' + id;
    var s = $(selectId);

    s.append('<div class="overflow"></div> ');
    s.css('z-index', editDownloadIndex);
    editDownloadIndex++;
    if (s.find('div.row').length === 0) {
        s.find('.overflow').append('<div class="row">' +
            '<div class="roundedOne">' +
            ' <input type="checkbox"value="" class="allDealer" id="allDealer' + selectId + '" name="dealerCheckbox"  />' +
            ' <label for="allDealer' + selectId + '"></label></div>Choose all dealers</div><br>');
        for (var val in dealers) {
            var dealerId = dealers[val]['id'];
            var dealerName = dealers[val]['dealer'];
            s.find('.overflow').append('<div class="row">' +
                '<div class="roundedOne">' +
                ' <input type="checkbox"value="' + dealerId + '" class="dealerCheckboxInput" id="roundedOne' + selectId + val + '" name="dealerCheckbox"  />' +
                ' <label for="roundedOne' + selectId + val + '"></label></div>' + dealerName + '</div>');
        }

    }
    selectAlldealers(selectId);
    toggleDealerCheckbox(selectId);
    calcCountDealers(selectId);


    s.find('.overflow').niceScroll({
        cursorcolor: "#00838f",
        cursorwidth: "15px",
        background: "#e0e0e0",
        cursorborder: "1px solid #e0e0e0",
        cursorborderradius: 0

    }).rail.css({
        "z-index": 9999,
        "margin-left": "2px"
    });

    // if (s.find('.jspScrollable').length === 0) {
    //     s.find('.overflow').jScrollPane();
    // }
}


function calcCountDealers(selectId) {

    var inputs = $(selectId).find('.dealerCheckboxInput');
    var count = 0;
    for (var key in inputs) {
        if (!inputs.hasOwnProperty(key)) {
            //The current property is not a direct property of p
            continue;
        }
        $(inputs[key]).find('input').change(function (e) {
            count = $(selectId).find('.dealerCheckboxInput:checked').length;
            $(selectId).parent().parent().find('.dealerNumberFile').html(count);

        })
    }
}


function newFilesToServer() {
    $('.createFileBtn').click(function () {
        var filesInTable = $('.template-upload');
        var downloadFiles = [];
        filesInTable.each(function (index) {
            var FileName = $(this).find('.name').text();
            var dealer = $(this).find('.dealerCheckboxInput:checked');
            var dealersList = [];
            dealer.each(function (index) {
                dealersList.push({
                    id: $(this).val()
                })
            });
            downloadFiles.push({
                fileName: FileName,
                dealers: dealersList
            });

        });
        downloadFiles = JSON.stringify(downloadFiles);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                mycallback(request.responseText); // Another callback here
            }
        };
        setTimeout(function () {
            request.open("POST", "add_new_files/dealers");
            request.send(downloadFiles);
        }, 1000);


    })
}

function mycallback(data) {
    // location.reload();
    location.replace(mainUrl + '/download/admin')
    // showToastr('Files', data['message'])
}