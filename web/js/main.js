var addons = {
    'smarthdflex': 'SmartView HD Flex',
    'front': 'Front Camera',
    'dynamiclines': 'Dynamic Parking Lines',
    'toolkit': 'Trim Tool Kit',
    'interface_hd': 'Multimedia Interface HD upgrade',
    'smarthd': 'SmartView HD',
    'vividscreen': 'VividScreen',
    'vividscreen_smarthd': 'VividScreen+SmartView HD',
    'warranty': 'Extended Warranty',
    'rear_cam': 'Rear View Camera'
};
var mainUrl = window.location.origin;

var blockAccount = $('#account');
var accountName = $('#showAccount');
(function ($) {
    $(document).ready(function () {
        hljs.initHighlightingOnLoad();
        getNotifications();
        login();
        showForgot();
        hideForgot();
        showAccount();
        showChangePass();
        hideAccount();
        showEditAcoount();
        showCheckProduc();
        showCheckMMI();
        showWhatCheck();
        spotErrorForm();
        changeAccountPass();
        editDownloadFiler();
        showNewDelaer();
        showEditDealer();
        showNewOrder();
        showChangeManeger();
        showCreateFile();
        deleteDealer();
        // selectAlldealers();
        showEditOrder();
        showCreateManager();
        deleteDealerFormShow();
        newDealerFormShow();
        newManagerFormShow();
        deleteManagerFormShow();
        vinCheck();
        askDelManager();
        askDelDealer();
        seeRecentlyAllDealers();
        showDealersBtn();
        showSecurityMainForm();
        showProfileInfoForm();
        showManagerDealers();
        squarePhoto('.profileFoto');
        showTableExport('.table');
        showExportData();
        showConfirmDelete();
        managerListShow();
        squareNews();
        sendComment();
        tinymce.init({
            selector: '#appbundle_post_content',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        });

        tinymce.init({
            selector: '#appbundle_post_edit_content',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        });
        // $('body').niceScroll();
        // capitalizeFirstLetter(string);
        // Datetime picker initialization.
        // See http://eonasdan.github.io/bootstrap-datetimepicker/
        loadDatePicker();
        $(".chosen-select").chosen({
            width: "100%",
            no_results_text: "Oops, nothing found!",
            placeholder_text_single: 'Choose country'
        });
        // Bootstrap-tagsinput initialization
        // http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/
        var $input = $('input[data-toggle="tagsinput"]');
        if ($input.length) {
            var source = new Bloodhound({
                local: $input.data('tags'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: Bloodhound.tokenizers.whitespace
            });
            $input.tagsinput({
                trimValue: true,
                focusClass: 'focus',
                typeaheadjs: {
                    name: 'tags',
                    source: source
                }
            });
        }
    });

    // Handling the modal confirmation message.
    $(document).on('submit', 'form[data-confirmation]', function (event) {
        var $form = $(this),
            $confirm = $('#confirmationModal');

        if ($confirm.data('result') !== 'yes') {
            //cancel submit event
            event.preventDefault();

            $confirm
                .off('click', '#btnYes')
                .on('click', '#btnYes', function () {
                    $confirm.data('result', 'yes');
                    $form.find('input[type="submit"]').attr('disabled', 'disabled');
                    $form.submit();
                })
                .modal('show');
        }
    });

    $('#fogotForm').submit(function (e) {
        e.preventDefault();
        var email = $('#usernameFogot').val();
        // $('.checkForgot').css("display","block");
        var mesBlock = $('.checkForgot');
        mesBlock.css("display", "block");

        $.ajax({
            url: 'api/resetting/request',
            method: 'POST',
            timeout: 5000,
            data: {
                email: email
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                $(mesBlock).find('p').text(data);
            }
        });
    });

    function askDelManager() {
        var btn = $('#mYesBtn');
        $('#appbundle_delete_usermanager_manager option').click(function (e) {
            e.preventDefault();
            btn.hide();
        });
        $('#mDelBtn').click(function (e) {
            btn.hide();
            e.preventDefault();
            // $.playSound(mainUrl + '/sound/sure.wav');
            var v = $('#appbundle_delete_usermanager_manager').val();
            if (v != '') {
                btn.hide();
                btn.show();
            }
        });

    }

    function seeRecentlyAllDealers() {
        $('.seeRecentlyAllDealers').click(function (e) {
            e.preventDefault();
            var btnAll = $('#seeAllDealers');
            var btnRecently = $('#seeRecentlyDealers');
            var resently = $(this).data('resently');
            var showDealers = $('#showDealers');
            showDealers.removeClass('tableOverflow');


            $.ajax({
                url: '/admin/account/all/dealers',
                method: 'GET',
                timeout: 5000,
                data: {
                    resently: resently
                },
                error: function () {
                    console.log('error');
                },
                success: function (data) {
                    showDealers.find('table').html(data);
                    var total = 0;
                    $('.dealerValue').each(function (index, elem) {
                        total += parseInt($(elem).text());
                    });
                    btnAll.removeClass('allDealersOptActive');
                    btnAll.addClass('allDealersOptDisable');
                    btnRecently.removeClass('allDealersOptActive');
                    btnRecently.removeClass('allDealersOptDisable');
                    $('#totalOrderSum').html('$ ' + total);
                    // added/remove color scroll
                    if (!resently) {
                        showDealers.niceScroll({
                            cursorcolor: "#00838f",
                            cursorwidth: "15px",
                            background: "#e0e0e0",
                            cursorborder: "1px solid #e0e0e0",
                            cursorborderradius: 0

                        }).rail.css({
                            "z-index": 9999,
                            "margin-left": "2px"
                        });
                        showDealers.addClass('tableOverflow');
                    } else {
                        showDealers.getNiceScroll().remove();
                    }
                    showTableExport('.table');
                }
            });
        });
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function showDealersBtn() {

        $('#seeAllDealersBtn').on("click", function () {
            $('#recentlyAddedDealersBtn').removeClass('hidden');
            $('#seeAllDealersBtn').addClass('hidden');
            $('#seeAllDealersP').removeClass('hidden');
            $('#recentlyAddedDealersP').addClass('hidden');
        });
        $('#recentlyAddedDealersBtn').on("click", function () {
            $('#seeAllDealersBtn').removeClass('hidden');
            $('#seeAllDealersP').addClass('hidden');
            $('#recentlyAddedDealersP').removeClass('hidden');
            $('#recentlyAddedDealersBtn').addClass('hidden');
        });
    }

    function showManagerDealers() {

        $('.seeManagerDealers').click(function (e) {
            e.preventDefault();
            var resently = $(this).data('resently');
            var showDealers = $('#showDealers');
            $.ajax({
                url: '/manager/account/all/dealers',
                method: 'GET',
                timeout: 5000,
                data: {
                    resently: resently
                },
                error: function () {
                    console.log('error');
                },
                success: function (data) {


                    showDealers.find('table').html(data);
                    var total = 0;
                    $('.dealerValue').each(function (index, elem) {
                        total += parseInt($(elem).text());
                    });
                    $('#managerRecDealers').on("click", function () {
                        $(this).addClass('allDealersOptActive');
                        $('#managerAllDealers').removeClass('allDealersOptActive ');
                    });
                    $('#managerAllDealers').on("click", function () {
                        $(this).addClass('allDealersOptActive');
                        $('#managerRecDealers').removeClass('allDealersOptActive ');
                    });
                    $('#totalOrderSum').html('$ ' + total);
                    // // added/remove color scroll
                    if (!resently) {
                        showDealers.niceScroll({
                            cursorcolor: "#00838f",
                            cursorwidth: "15px",
                            background: "#e0e0e0",
                            cursorborder: "1px solid #e0e0e0",
                            cursorborderradius: 0

                        }).rail.css({
                            "z-index": 9999,
                            "margin-left": "2px"
                        });
                        showDealers.addClass('tableOverflow');
                    } else {
                        showDealers.getNiceScroll().remove();
                    }
                    showTableExport('.table');
                }
            });
        });
    }

    function showSecurityMainForm() {
        var account = $('#accountMainForm');
        var security = $('#securityMainForm');
        $('#securityMainFormBtn').on("click", function () {
            account.css("display", "none");
            security.css("display", "block");
        });
        $('#accountMainFormBtn').on("click", function () {
            account.css("display", "block");
            security.css("display", "none");
        });
        $('#changeOldPassBtn').on("click", function () {
            $('#confirmPass').removeClass('hidden');
        });
    }

    function showProfileInfoForm() {
        var personal = $('#accountMainForm');
        var account = $('#securityMainForm');
        var editform = $('.editForm');
        // $('#personalInfoBtn').css({'background-color': 'transparent', 'border': '1px solid #00838f'});
        $('#personalInfoBtn').addClass('persInfoActive');
        $('#personalAccountBtn').removeClass('persInfoDsiable');
        $('#personalInfoBtn').on("click", function () {
            personal.css("display", "block");
            account.css("display", "none");
            editform.css("display", "none");
            $('#accountEditBtn').css("display", "block");
            $('#personalAccountBtn').removeClass('persInfoActive');
            $('#personalAccountBtn').addClass('persInfoDsiable');
            $(this).addClass('persInfoActive');
        });
        $('#personalAccountBtn').on("click", function () {
            account.css("display", "block");
            personal.css("display", "none");
            editform.css("display", "none");
            $('#accountForm').css("display", "none");
            $('#personalInfoBtn').removeClass('persInfoActive');
            $('#personalInfoBtn').removeClass('persInfoDisable');
            $(this).removeClass('persInfoDisable');
            $(this).addClass('persInfoActive');
        });
        $('#changeOldPassBtn').on("click", function () {
            $('#confirmPass').removeClass('hidden');
        });
    }

    $(document).click(function (event) {
        var mesBlock = $('.messageBlock');
        if (!$(event.target).closest(mesBlock).length) {
            if ($(mesBlock).is(":visible")) {
                hideMessage(mesBlock);
            }
        }
    });

})(window.jQuery);

function loadDatePicker() {
    $('[data-toggle="datetimepicker"]').datetimepicker({
        maxDate: 'now',
        widgetPositioning: {
            horizontal: 'left',
            vertical: 'bottom'
        },
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-check-circle-o',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });
}


function showForgot() {
    $('#showHide').click(function () {
        $('#disableBlock').removeClass('hidden');
        $('#fogotForm').fadeIn('slow');
        $('#authForm').css("display", "none");
        $('#disableBlock').css("display", "none");
    })
}
function hideForgot() {
    $('#hideForgot').click(function () {
        $('#disableBlock').addClass('hidden');
        $('#fogotForm').css("display", "none");
        $('#authForm').css("display", "block");
        $('.badPass').css("display", "none");
    })
}

function showMessage(block) {
    block.css("opacity", 1);
    $('.homeContaiter').addClass('hidebg fadeIn');
}
function hideMessage(block) {
    block.css("opacity", 0);
    $('.homeContaiter').removeClass('hidebg');
}


function askDelDealer() {
    var btn = $('#dYesBtn');
    $('#appbundle_delete_userdealer_dealer option').click(function (e) {
        e.preventDefault();
        btn.hide();
    });
    $('#dDelBtn').click(function (e) {
        btn.hide();
        e.preventDefault();
        // $.playSound(mainUrl + '/sound/sure.wav');
        var v = $('#appbundle_delete_userdealer_dealer').val();
        if (v != '') {
            btn.hide();
            btn.show();
        }
    });
}

function seeAllDealers() {
    $('#seeAllDealers').click(function (e) {
        e.preventDefault();
    });
}
//
function showAccount() {
    $('#showAccount').click(function (e) {
        e.preventDefault();
        // $.playSound(mainUrl + '/sound/showAccount.wav');
        blockAccount.removeClass('slideOutRight');
        blockAccount.removeClass('hidden');
        // $('#personalInfoBtn').css({'background-color': 'transparent', 'border': '1px solid #00838f'});
        $('#personalInfoBtn').addClass('persInfoActive');
        // $('#personalAccountBtn').css({'background-color': '#00bcd4', 'border': 'none'});
        $('#personalAccountBtn').addClass('persInfoDisable');
        $('#newDealerFormBtn').removeClass('persInfoDisable');
        $('#newDealerFormBtn').addClass('persInfoActive');
        $('#deleteDealerFormBtn').removeClass('persInfoActive');
        $('#deleteDealerFormBtn').addClass('persInfoDisable');
        $('#securityMainForm').css("display", "none");
        squarePhoto('.profileFoto');

        closeAny('#account', '#showAccount', 'slideOutRight');
    })
}

function showNewDelaer() {
    $('#addNewDealer').click(function (e) {
        e.preventDefault();
        $('#addNewDealerBlock').removeClass('slideOutRight');
        $('#addNewDealerBlock').removeClass('hidden');

        closeAny('#addNewDealerBlock', '#addNewDealer', 'slideOutRight');
    })
}
function showNewOrder() {
    $('#newOrderBtn').click(function (e) {
        e.preventDefault();
        $('#newOrder').removeClass('slideOutRight');
        $('#newOrder').removeClass('hidden');
        closeAny('#newOrder', '#newOrderBtn', 'slideOutRight');
    })
}
function showChangeManeger() {
    $('#changeManagerBtn').click(function (e) {
        e.preventDefault();
        $('#changeManager').removeClass('slideOutRight');
        $('#changeManager').removeClass('hidden');
        closeAny('#changeManager', '#changeManagerBtn', 'slideOutRight');
    })
}
function showCreateFile() {
    $('.createFileBlockBtn').click(function (e) {
        e.preventDefault();
        $('#newFileBlock').removeClass('slideOutDown');
        $('#newFileBlock').removeClass('hidden');
        closeAny('#newFileBlock', '.createFileBlockBtn', 'slideOutDown');
        $('.closeNewfile').click(function (e) {
            e.preventDefault();
            $('#newFileBlock').addClass('slideOutDown');
            setTimeout(function () {
                $('#newFileBlock').addClass('hidden');

            }, 1000)
        })
    })
}
function showCreateManager() {
    $('#addNewManagerBtn').click(function (e) {
        e.preventDefault();
        $('#addNewManager').removeClass('slideOutRight');
        $('#addNewManager').removeClass('hidden');

        closeAny('#addNewManager', '#addNewManagerBtn', 'slideOutRight');
    })
}


function hideAccount() {
    $('.closeAccount').click(function (e) {
        e.preventDefault();
        // $.playSound(mainUrl + '/sound/closeAccount.wav');
        blockAccount.addClass('slideOutRight');
        $('#addNewDealerBlock').addClass('slideOutRight');
        $('#editBlock').addClass('slideOutRight');
        $('#newOrder').addClass('slideOutRight');
        $('#changeManager').addClass('slideOutRight');
        $('#createFile').addClass('slideOutRight');
        $('#editOrder').addClass('slideOutRight');
        $('#addNewManager').addClass('slideOutRight');
        $('#editManager').addClass('slideOutRight');
        setTimeout(function () {
            blockAccount.addClass('hidden');
            $('#addNewDealerBlock').addClass('hidden');
            $('#editBlock').addClass('hidden');
            $('#newOrder').addClass('hidden');
            $('#changeManager').addClass('hidden');
            $('#createFile').addClass('hidden');
            $('#editOrder').addClass('hidden');
            $('#addNewManager').addClass('hidden');
            $('#editManager').addClass('hidden');
            hideEditAcoount();
        }, 1000)

    })
}
function showChangePass() {
    $('#changePassBtn').click(function (e) {
        e.preventDefault();
        $('#accountForm').toggle("slow", function () {
        });
    });
}

function showEditAcoount() {
    $('#accountEditBtn').click(function (e) {
        e.preventDefault();
        $('.editForm').show();
        // $('#accountEditBtn').fadeOut();
        // $('.accountInfo').fadeOut();
        $('#accountEditBtn').css("display", "none");
        $('.accountInfo').css("display", "none");
    })
}


function hideEditAcoount() {
    $('.editForm').hide();
    $('#accountEditBtn').fadeIn();
    $('.accountInfo').fadeIn();
}

function showCheckProduc() {
    $('#checkProduct').click(function () {
        $('#checkProductBlock').show();
        $('#whatCheck').hide();
    })
}

function showWhatCheck() {
    $('.goBack').click(function () {
        $('#whatCheck').show();
        $('#checkProductBlock').hide();
        $('#checkMMIBlock').hide();
        $('.vincheckError').hide();
        $('.vincheckProducts').children('.row').remove();
    })
}
function showCheckMMI() {
    $('#checkMMI').click(function () {
        $('#checkMMIBlock').show();
        $('#whatCheck').hide();
    })
}

function getProducts(vin) {
    // $('#checkProductBtn').submit(function (e) {
    //     e.preventDefault();
    var productsBlock = $('.vincheckProducts');
    var detaisBlock = $('.vincheckDetails');
    $('.vincheckErrorProducts').hide();
    productsBlock.children('.row').remove();
    productsBlock.html('<div><img src="../../web/img/loader.gif"> Finding  products</div>');
    $.ajax({
        url: 'check/products/vin',
        method: 'GET',
        timeout: 10000,
        data: {
            vin: vin
        },
        error: function () {
            productsBlock.html('');
            $('.vincheckErrorProducts').show().text('Error connecting to the server. Please try again.')
        },
        success: function (data) {
            getMMI(vin);
            $('.spotErrorBlock').show();
            productsBlock.html('');
            data = jQuery.parseJSON(data);
            productsBlock.html('');
            detaisBlock.html('');


            if (data.error != null) {
                data = Object.values(data.error);
                console.log(data);
                // $('.vincheckErrorProducts').show().text(data[0]);
                $('.vincheckErrorProducts').show().text('No compatible products found for this VIN.');
            } else {
                var model = data.vinData["E series"].split(' ');
                printVinCheck(model[0]);
                detaisBlock.append('<div class="row"><h4>Vin Details</h4></div>');
                detaisBlock.append('<div class="row" data-model="' + model[0] + '">' + data.vinData["VIN"] + '</div>');
                detaisBlock.append('<div class="row" data-model="' + model[0] + '">Model: ' + model[0] + '</div>');
                detaisBlock.append('<div class="row">Series: ' + data.vinData["Series"] + '</div>');
                detaisBlock.append('<div class="row">Type: ' + data.vinData["Type"] + '</div>');
                detaisBlock.append('<div class="row">Prod. date: ' + data.vinData["Prod.date"] + '</div>');
                var html = $('.vincheckProducts');
                html.show();
                html.append('<div class="row"><h4>Products compatible with your VIN</h4></div>');
                data.products.forEach(function (item, i, data) {
                    var show = true;

                    item.custom.forEach(function (p1, p2, p3) {
                        if (p1.error !== undefined) {
                            show = false;
                        }
                    });
                    if (show) {
                        html.append('<div class="row"><b>' + item.name + '</b> </div>');

                        if (item.custom.length > 0) {
                            html.append('<div class="row vincheckAddon">Available add-ons: </div>');
                        }
                        item.custom.forEach(function (product, j, data) {
                            var add = product.addon_key;
                            if (product['addon_key']) {
                                html.append('<div class="row vincheckAddon ">' + addons[add] + ' </div>');
                            }
                        })
                    }

                });
                $('#printVincheckBtn').show();
            }

        }
    });
    // })
}


function getMMI(vin) {
    // $('#checkMMIBtn').submit(function (e) {
    //     e.preventDefault();
    var productsBlock = $('.vincheckMMI');
    productsBlock.children('.row').remove();
    $('.vincheckErrorMMI').hide();
    productsBlock.html('<div><img src="../../web/img/loader.gif"> Finding parts</div>');
    $.ajax({
        url: 'check/mmi/vin',
        method: 'GET',
        timeout: 10000,
        data: {
            vin: vin
        },
        error: function () {
            console.log('error');
            productsBlock.html('');
            $('.vincheckErrorMMI').show().text('Error connecting to the server. Please try again.')
        },
        success: function (data) {
            productsBlock.html('');
            data = jQuery.parseJSON(data);
            // console.log(data);
            if (data.error != null) {
                // data = Object.values(data.error);
                // $('.vincheckErrorMMI').show().text(data.error);
                $('.vincheckErrorMMI').show().text('No compatible MMI for this VIN.');
            } else {
                var html = $('.vincheckMMI');
                html.show();

                html.append('<div class="row"><h4>MMI compatible with your VIN</h4></div>');
                // data.parts.forEach(function (item, i, data) {
                //     html.append('<div class="row">' + item.name + '</div>');
                // });
                html.append('<div class="row">' + data.parts[0].name + '</div>');

            }
        }
    })
    ;
}


function vinCheck() {
    $('#vinCheckForm').submit(function (e) {
        e.preventDefault();

        $('.spotErrorBlock').hide();
        $('.vincheckErrorDetails').children('.row').remove();
        $('.vincheckDetails').children('.row').remove();
        $('.vincheckErrorProducts').children('.row').remove();
        $('.vincheckProducts').children('.row').remove();
        $('.vincheckErrorMMI').children('.row').remove();
        $('.vincheckMMI').children('.row').remove();
        var vin = $('#inputCheckVin').val();

        getProducts(vin);

    })
}


function spotErrorForm() {
    $('#spotErrorForm').submit(function (e) {
        e.preventDefault();
        var vin = $('#inputCheckVin').val();
        var name = $('.dataCompany').data('company');
        var message = $('#spotErrorForm textarea').val();

        $.ajax({
            url: 'send/mistake',
            method: 'POST',
            timeout: 5000,
            data: {
                vin: vin,
                name: name,
                message: message
            },
            error: function () {
                console.log('error');

            },
            success: function (data) {
                showToastr('Message', data, "toast-top-full-width", 2000);
                $('#spotErrorForm textarea').val('');
            }
        })

    })
}


function sendComment() {
    $('#tellNewsForm').submit(function (e) {
        e.preventDefault();
        var article = $('.contentNewsBlock h1').text();
        var mail = $('.dataEmail').data('email');
        var message = $('#tellNewsForm textarea').val();

        console.log(article);
        console.log(mail);
        console.log(message);

        $.ajax({
            url: mainUrl + '/blog/send/comment',
            method: 'POST',
            timeout: 5000,
            data: {
                mail: mail,
                article: article,
                message: message
            },
            error: function () {
                console.log('error');

            },
            success: function (data) {
                showToastr('Message', data, "toast-top-full-width", 2000);
                $('#tellNewsForm textarea').val('');
            }
        })

    })
}


function changeAccountPass() {
    $('form[name="reset_password"]').submit(function (e) {
        e.preventDefault();
        var formSerialize = $(this).serialize();
        $.post('', formSerialize, function (response) {
            //your callback here
            showToastr('Password', response, 'toast-bottom-right');

            $('#accountForm').hide();
            $("form").trigger("reset");
        }, 'JSON');
    })
}


function showEditDealer() {
    $('.editBlockBtn').click(function (e) {
        e.preventDefault();
        var user = $(this).data('id');
        var editBlock = $('#editBlock');
        var mainUrl = window.location.origin;
        editBlock.html('<div class="accountBlock"><img src="' + mainUrl + '/web/img/loader.gif"> Finding User</div>');
        $.ajax({
            // url: 'user/by/' + user,
            url: '',
            method: 'POST',
            timeout: 5000,
            data: {
                user: user
            },
            error: function () {

                editBlock.html('<div class="accountBlock">Error connecting to the server. Please try again.</div>')
            },
            success: function (data) {
                var editblock = $(data).find('div#wrapperEdit');
                editBlock.html(editblock);
                hideAccount();
            }
        });
        editBlock.removeClass('slideOutRight');
        editBlock.removeClass('hidden');

    });

}


function showEditOrder() {
    $('.editBlockAjaxBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).data('url');
        var user = $(this).data('userid');
        var role = url.split("/")[0];
        var editBlock = $('.editBlockAjax');

        editBlock.html('<div class="accountBlock"><img src="' + mainUrl + '/web/img/loader.gif"> Finding ' + role + '</div>');
        $.ajax({
            // url: 'user/by/' + user,
            url: url + '/edit/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id
            },
            error: function () {
                editBlock.html('<div class="accountBlock">Error connecting to the server. Please try again.</div>')
            },
            success: function (data) {

                closeAny('#editOrder', '.editOrderBtnbtn', 'slideOutRight');
                editBlock.html(data);
                hideAccount();
                loadDatePicker();
                if (role == 'order') {
                    editOrder(url, id, user)
                } else if (role == 'managers') {
                    editManager(url, id, user)
                }

            }
        });
        editBlock.removeClass('slideOutRight');
        editBlock.removeClass('hidden');
    });
}

function editOrder(url, id, user) {
    var editForm = $('form[name="appbundle_edit_dealer_order"]');
    editForm.submit(function (e) {
        e.preventDefault();
        var fields = $('form[name="appbundle_edit_dealer_order"] input').map(function () {
            return $(this).val();

        }).get();
        //
        // console.log( fields[0]);
        // console.log( fields[1]);
        // console.log( fields[2]);
        // console.log( fields[3]);
        // console.log( fields[4]);
        $.ajax({
            // url: 'user/by/' + user,
            url: url + '/edit/do/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                user: user,
                id: id,
                createAt: fields[0],
                value: fields[1],
                cost: fields[2],
                itemNumber: fields[3]
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                location.reload();
            }
        });
    })
}


function editManager(url, id, user) {
    var editForm = $('form[name="appbundle_edit_usermanager"]');
    editForm.submit(function (e) {
        e.preventDefault();
        var fields = $('form[name="appbundle_edit_usermanager"] input').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            // url: 'user/by/' + user,
            url: url + '/edit/do/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                user: user,
                id: id,
                fullName: fields[1],
                email: fields[2],
                phone: fields[3],
                // file: fields[4]
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                location.reload();
            }
        });
    })
}


function deleteDealer() {
    $('.deleteBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var url = $(this).data('url');
        // var userid = $(this).data('userid');
        var block = $(this);
        $.ajax({
            url: url + '/delete/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id
                // user: userid
            },
            error: function () {

            },
            success: function (data) {
                var deleteElement = block.parent().parent().prev();
                deleteElement.addClass('hinge');
                $('#confirmRow' + id).hide();
                setTimeout(function () {
                    deleteElement.addClass('hidden')

                }, 2000)
            }
        });

    })
}


function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        console.log('image');
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
        $('.upload-file-container-text').addClass('changeAvatart');
    }
}

$("#appbundle_usermanager_file").change(function () {
    readURL(this);
});

function readURLEdit(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        console.log('image');
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
        $('.upload-file-container-text').addClass('changeAvatart');
    }
}

$("#appbundle_edit_usermanager_file").change(function () {
    readURLEdit(this);
});


function deleteDealerFormShow() {
    var deleteBtn = $('#deleteDealerFormBtn');
    var newBtn = $('#newDealerFormBtn');
    deleteBtn.click(function (e) {
        e.preventDefault();
        $('#newDealerForm').fadeOut();
        $('#deleteDealerForm').fadeIn();
        // deleteBtn.removeClass('chooseOptDisable');
        // deleteBtn.addClass('chooseOptActive');
        // newBtn.removeClass('chooseOptActive');
        // newBtn.addClass('chooseOptDisable');
        deleteBtn.removeClass('persInfoDisable');
        deleteBtn.addClass('persInfoActive');
        newBtn.removeClass('persInfoActive');
        newBtn.addClass('persInfoDisable');

    })
}


function newDealerFormShow() {
    var deleteBtn = $('#deleteDealerFormBtn');
    var newBtn = $('#newDealerFormBtn');
    newBtn.click(function (e) {
        e.preventDefault();
        $('#newDealerForm').fadeIn();
        $('#deleteDealerForm').fadeOut();
        // deleteBtn.removeClass('chooseOptActive');
        // deleteBtn.addClass('chooseOptDisable');
        // newBtn.removeClass('chooseOptDisable');
        // newBtn.addClass('chooseOptActive');
        deleteBtn.removeClass('persInfoActive');
        deleteBtn.addClass('persInfoDisable');
        newBtn.removeClass('persInfoDisable');
        newBtn.addClass('persInfoActive');

    })
}


function deleteManagerFormShow() {
    var deleteBtn = $('#deleteManagerFormBtn');
    var newBtn = $('#newManagerFormBtn');
    var managerListBtn = $('#managerListBtn');
    deleteBtn.click(function (e) {
        e.preventDefault();
        $('#newManagerForm').hide();
        $('#deleteManagerForm').show();
        $('#managerListForm').hide();
        deleteBtn.removeClass('persInfoDisable');
        deleteBtn.addClass('persInfoActive');
        newBtn.removeClass('persInfoActive');
        newBtn.addClass('persInfoDisable');
        managerListBtn.addClass('persInfoDisable');
        managerListBtn.removeClass('persInfoActive');

    })
}


function newManagerFormShow() {
    var deleteBtn = $('#deleteManagerFormBtn');
    var newBtn = $('#newManagerFormBtn');
    var managerListBtn = $('#managerListBtn');
    managerListBtn.click(function (e) {
        e.preventDefault();
        $('#managerListForm').show();
        $('#newManagerForm').hide();
        $('#deleteManagerForm').hide();
        newBtn.removeClass('persInfoActive');
        newBtn.addClass('persInfoDisable');
        deleteBtn.removeClass('persInfoActive');
        deleteBtn.addClass('persInfoDisable');
        managerListBtn.addClass('persInfoActive');
        managerListBtn.removeClass('persInfoDisable');

    })
}


function managerListShow() {
    var deleteBtn = $('#deleteManagerFormBtn');
    var newBtn = $('#newManagerFormBtn');
    var managerListBtn = $('#managerListBtn');
    newBtn.click(function (e) {
        e.preventDefault();
        $('#newManagerForm').show();
        $('#deleteManagerForm').hide();
        $('#managerListForm').hide();
        deleteBtn.removeClass('persInfoActive');
        deleteBtn.addClass('persInfoDisable');
        newBtn.removeClass('persInfoDisable');
        newBtn.addClass('persInfoActive');
        managerListBtn.addClass('persInfoDisable');
        managerListBtn.removeClass('persInfoActive');

    })
}


function getNotificationCount(id) {
    $.ajax({
        url: mainUrl + '/notification/all/count/' + id,
        method: 'POST',
        timeout: 5000,
        data: {
            id: id
        },
        error: function () {
            console.log('error');
        },
        success: function (data) {
            $('.notification a').addClass('danger-block').text(data);
        }
    });
}

function showToastr(title, message, position, timeOut) {

    if (position === undefined) {
        position = "toast-bottom-left"
    }
    if (timeOut === undefined) {
        timeOut = "0"
    }
    $.playSound(mainUrl + '/sound/message2.wav');
    toastr.options.onclick = function () {
        $.playSound(mainUrl + '/sound/messageClose.wav');
    };
    toastr.success(message, title, {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": position,
        // "showDuration": "3000",
        "preventDuplicates": false,
        "timeOut": timeOut,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "slideDown",
        "hideMethod": "slideUp"

    });
}

function IsSafari() {
    isSafari = navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent); // true or false
    return isSafari;
}


function addDealerToFile() {
    $('.addDealerToFile').click(function (e) {
        $.ajax({
            url: 'add_new_file/dealers',
            method: 'POST',
            timeout: 5000,
            error: function () {
                console.log('error');
            },
            success: function (data) {
            }
        });
    });
}
function showExportData() {

    $('#showExportData').click(function (e) {
        e.preventDefault();
        $('.tableexport-caption').toggleClass('hidden');
        setTimeout(function () {
            closeAny('.tableexport-caption', '#account', null, 100);
        }, 100);
        $('#showExportData').click(function (e) {
            e.stopPropagation();
        });
    });

}

function showTableExport(tableExport) {
    $(tableExport).tableExport({
        headings: true,                    // (Boolean), display table headings (th/td elements) in the <thead>
        footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
        formats: ["xls", "csv", "txt"],    // (String[]), filetypes for the export
        fileName: "id",                    // (id, String), filename for the downloaded file
        bootstrap: true,                   // (Boolean), style buttons using bootstrap
        position: "top",                   // (top, bottom), position of the caption element relative to table
        ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file(s)
        ignoreCols: null,                  // (Number, Number[]), column indices to exclude from the exported file(s)
        ignoreCSS: "",                       // (selector, selector[]), selector(s) to exclude from the exported file(s)
        emptyCSS: "",                     // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s)
        trimWhitespace: false              // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s)
    });
    $('.tableexport-caption').addClass('hidden');
}
//manager photo make circle
function squarePhoto(profileFoto) {
    $(profileFoto).height($(profileFoto).width());
}


function squareNews() {
    var blockNews = $('.blockNews');
    // var blockPromotion = $('.blockPromotion');
    var width = blockNews.width();
    if (width != null) {
        $('.thumbnailPhoto').height(width);
    } else {
        $('.thumbnailPhoto').height($('.blockPromotion').width() / 2);
    }


    // blockPromotion.height(width);

}


function getNotifications() {
    $('.notification').click(function (e) {
        e.preventDefault();

        var notList = $('#notificationList');


        $.ajax({
            url: mainUrl + '/notification/all/notification',
            method: 'POST',
            timeout: 5000,

            error: function () {
                console.log('error');
            },
            success: function (data) {
                notList.html('');
                data = jQuery.parseJSON(data);
                notList.append('<li class="notTitle"> YOUR NOTIFICATIONS</li>');
                data.forEach(function (item, i, arr) {
                    var isRead = '';
                    if (!item.has_read) {
                        isRead = "notRead";
                    }
                    notList.append('<li class="' + isRead + '" data-id="' + item.id + '">' +
                        '<div class="row greyTxt"> ' + MDFormat(item.create_at) + '</div>' +
                        '<div class="row colText"> ' + item.title + '</div>' +
                        '<div class="row">' + item.message + '</div>' +
                        '</li>')
                });
                notList.show();
                notList.niceScroll({
                    cursorcolor: "#212121",
                    cursorwidth: "15px",
                    background: "#757575",
                    cursorborder: "1px solid #757575",
                    cursorborderradius: 0

                }).rail.css({
                    "z-index": 9999,
                    "margin-left": "2px"
                });


                readNot();
                anyCliclClose('#notificationList');

                if (notList.is(':visible')) {
                    $('.notification a').addClass('notTriangle');
                } else {
                    $('.notification a').removeClass('notTriangle');
                }
            }
        });

    })
}

function readNot() {
    $('.notRead').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');

        $.ajax({
            url: mainUrl + '/notification/all/notification/read/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                $(this).removeClass('notRead');
                console.log(data);
                if (data > 0) {
                    $('.notification a').addClass('danger-block').text(data);
                } else {
                    $('.notification a').removeClass('danger-block').text(data);
                }


            }
        });
    })
}


function MDFormat(MMDD) {
    MMDD = new Date(MMDD.replace('+', '.'));
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var strDate = "";
    var today = Date.today();
    today.setHours(0, 0, 0, 0);
    var yesterday = Date.parse('t - 1 d');
    yesterday.setHours(0, 0, 0, 0);
    if (today.getTime() <= MMDD.getTime()) {
        strDate = "Today";
    } else if (yesterday.getTime() <= MMDD.getTime()) {
        strDate = "Yesterday";
    } else {
        strDate = months[MMDD.getMonth()] + "-" + MMDD.getDate();
    }
    return strDate;
}


function anyCliclClose(closeBlock) {


    if (IsSafari()) {
        $(document).bind("touchstart", function (event) {
            if (!$(event.target).closest(closeBlock).length) {
                if ($(closeBlock).is(":visible")) {
                    $(closeBlock).hide();
                    $('.notification a').removeClass('notTriangle');
                    $('#notificationList').getNiceScroll().remove();
                }
            }
        });
    } else {
        $(document).click(function (event) {
            if (!$(event.target).closest(closeBlock).length) {
                if ($(closeBlock).is(":visible")) {
                    $(closeBlock).hide();
                    $('.notification a').removeClass('notTriangle');
                    $('#notificationList').getNiceScroll().remove();
                }
            }
        });
    }
}


function ifIOS() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    return iOS;
}
function closeAny(closeBlock, clickBlock, animate, time) {


    $(document).click(function (e) {
        if (!$(e.target).parents().andSelf().is(clickBlock) && $(closeBlock).is(":visible")) {
            if (animate != undefined) {
                $(closeBlock).addClass(animate);
            }
            if (time === undefined) {
                time = 1000;
            }
            setTimeout(function () {
                $(closeBlock).addClass('hidden');
            }, time);
            $(document).off('click');
        }
    });


    $(closeBlock).click(function (e) {
        e.stopPropagation();
    });


}

function toggleDealerCheckbox(selectId) {
    $(selectId).toggle();
}


function selectAlldealers(id) {
    $(id + ' .allDealer').click(function (e) {
        if ($(id + ' .allDealer').is(':checked')) {
            $(id + ' .roundedOne input').each(function () {
                $(this).prop('checked', true)
            })
        } else {
            $(id + ' .roundedOne input').each(function () {
                $(this).prop('checked', false)
            })
        }
    });
}
var editDownloadIndex = 10;
function editDownloadFiler() {

    $('.editFileBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var selectId = '#selectDealer' + id;
        var dealerFileCount = $('#dealerFileCount' + id);
        var dealersListA = null;
        var dealersListR = null;
        var save = $(selectId).is(":visible");
        if ($(selectId).find('.row').length !== 0) {
            var dealerA = $(selectId).find('.dealerCheckboxInput:checked');
            dealersListA = [];
            dealerA.each(function (index) {
                dealersListA.push(
                    $(this).val()
                )
            });
            var dealerR = $(selectId).find('.dealerCheckboxInput').not(':checked');
            dealersListR = [];
            dealerR.each(function (index) {
                dealersListR.push(
                    $(this).val()
                )
            });

        }

        $.ajax({
            url: mainUrl + '/download/edi_file/dealers/' + id,
            method: 'POST',
            timeout: 5000,
            data: {
                id: id,
                dealersAdd: dealersListA,
                dealersRemove: dealersListR,
                save: save
            },
            error: function () {
                console.log('error');
            },
            success: function (data) {
                var count = data.fileDealers.length;
                dealerFileCount.html(count);
                toggleDealerCheckbox(selectId);
                console.log(data);

                var s = $(selectId);
                s.css('z-index', editDownloadIndex);
                editDownloadIndex++;
                s.append('<div class="overflow"></div> ');
                if (s.find('div.row').length === 0) {
                    s.find('.overflow').append('<div class="row">' +
                        '<div class="roundedOne">' +
                        ' <input type="checkbox"value="" class="allDealer" id="allDealer' + selectId + '" name="dealerCheckbox"  />' +
                        ' <label for="allDealer' + selectId + '"></label></div>Choose all dealers</div><br>');
                    for (var val in data.userDealers) {

                        var dealerId = data.userDealers[val].id;
                        var dealerName = data.userDealers[val].company;
                        var checked = '';
                        for (var file in data.fileDealers) {
                            if (dealerId === data.fileDealers[file].id) {
                                checked = 'checked';
                            }
                        }
                        s.find('.overflow').append('<div class="row">' +
                            '<div class="roundedOne">' +
                            ' <input type="checkbox"value="' + dealerId + '" class="dealerCheckboxInput" id="roundedOne' + selectId + val + '" name="dealerCheckbox" ' + checked + '  />' +
                            ' <label for="roundedOne' + selectId + val + '"></label></div>' + dealerName + '</div>');
                    }

                }


                $(selectId).find('.overflow').niceScroll({
                    cursorcolor: "#00838f",
                    cursorwidth: "15px",
                    background: "#e0e0e0",
                    cursorborder: "1px solid #e0e0e0",
                    cursorborderradius: 0

                }).rail.css({
                    "z-index": 9999,
                    "margin-left": "2px"
                });

                selectAlldealers(selectId);
            }
        });
    })
}

function printVinCheck(model) {

    $('#printVincheck').click(function () {
        var company = $('.dataCompany').data('company');
        var printStr = company;
        if ($('.dataCity').data('city') != undefined) {
            var city = $('.dataCity').data('city');
            var phone = $('.dataPhone').data('phone');
            printStr = company + ', ' + city + ', ' + phone;
        } else if ($('.dataPhoneNUmber').data('phone_number') != undefined) {
            var email = $('.dataEmail').data('email');
            printStr = company + ', ' + email + ', ' + $('.dataPhoneNUmber').data('phone_number');
        }

        $('.printBlock').printThis({
            header: "<div class=''><div class='printLogoLeft'><p><img src='img/logo.png' ></p></div>" +
            // "<img src='img/logoText.png' width='70' height='50'></p></div>" +
            "<div class='printLogoRight'>" +
            "<p class='text-right'>" + printStr +
            "</div></div><h1>BimmerTech upgrades for your BMW " + model + "</h1>",
            importCSS: true,
            printContainer: false

        });
    });

}

function showConfirmDelete() {
    $('.confirmDeleteBtn').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('#confirmRow' + id).css("display", "table-row");
    })
    $('.cancelDelete1').click(function () {
        var id = $(this).data('id');
        $('#confirmRow' + id).hide();

    });
}